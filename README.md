# TYPO3 Base Minimal

A TYPO3 extension providing an integrated project/environment, except for fluid templates

## License

[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)

