<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\Tests\CropVariants;

use GrossbergerGeorg\BaseMinimal\CropVariants\CropVariantsLoader;
use GrossbergerGeorg\BaseMinimal\CropVariants\CropVariantsRenderer;
use GrossbergerGeorg\BaseMinimal\CropVariants\GalleryBuilder;
use GrossbergerGeorg\BaseMinimal\CropVariants\Variant;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Extbase\Domain\Model\FileReference as ExtbaseFileReference;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class GalleryBuilderTest extends TestCase
{
    /**
     * @dataProvider createGalleryDataProvider
     * @param bool $withReferences
     */
    public function testCreateGallery(bool $withReferences): void
    {
        $table = 'tt_content';
        $field = 'assets';

        $record = ['uid' => 1, 'pid' => 1];
        $rendererOptions = ['fast' => true];

        $ref1 = $this->createMock(FileReference::class);
        $ref1->expects(static::any())->method('getReferenceProperties')->willReturn(['uid' => 3]);
        $ref2 = $this->createMock(FileReference::class);
        $ref2->expects(static::any())->method('getReferenceProperties')->willReturn(['uid' => 4]);

        $references = [$ref1, $ref2];
        $loadedReferences = [];

        $files = $this->createMock(FileRepository::class);

        if ($withReferences) {
            $extbaseRef1 = $this->createMock(ExtbaseFileReference::class);
            $extbaseRef1->expects(static::any())->method('getOriginalResource')->willReturn($ref1);
            $loadedReferences = [
                $extbaseRef1,
                $ref2,
            ];
        } else {
            $files->expects(static::once())->method('findByRelation')->with(
                static::equalTo($table),
                static::equalTo($field),
                static::equalTo($record['uid'])
            )->willReturn($references);
        }

        $renderer = $this->createMock(CropVariantsRenderer::class);
        $renderer->expects(static::once())->method('mergeOptions')->with(static::equalTo($rendererOptions));

        $loader = $this->createMock(CropVariantsLoader::class);
        $loader->expects(static::any())->method('getForRecord')->willReturnMap([
            [$table, $field, $ref1->getReferenceProperties(), $record, ['variant1']],
            [$table, $field, $ref2->getReferenceProperties(), $record, ['variant2']],
        ]);

        $var1 = ['default' => new Variant(1, 2, '', 1)];
        $var2 = ['default' => new Variant(3, 4, '', 1)];

        $renderer->expects(static::any())->method('generateVariant')->willReturnMap([
            [$ref1, ['variant1'], $var1],
            [$ref2, ['variant2'], $var2],
        ]);

        $subject = new GalleryBuilder($loader, $renderer, $files);
        $subject->setTable($table);
        $subject->setField($field);

        $expected = [
            'default' => [
                [$var1['default']],
                [$var2['default']],
            ],
        ];
        $actual = $subject->createGallery($record, $rendererOptions, $loadedReferences);

        static::assertSame($expected, $actual);
    }

    public function createGalleryDataProvider()
    {
        return [
            'Without references argument' => [false],
            'With references argument'    => [true],
        ];
    }
}
