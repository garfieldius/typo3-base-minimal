<?php

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['base'] =
    'EXT:base_minimal/Configuration/RTE/Base.yml';
