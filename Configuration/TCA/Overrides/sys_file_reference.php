<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

$GLOBALS['TCA']['sys_file_reference']['columns']['crop']['config']['cropVariants'] = [
    'default' => [
        'title'               => 'LLL:EXT:base_minimal/Resources/Private/Language/locallang_db.xlf:crop.large',
        'allowedAspectRatios' => [
            'free' => [
                'title' => 'LLL:EXT:core/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                'value' => 0.0,
            ],
        ],
    ],
    'small_portrait' => [
        'title'               => 'LLL:EXT:base_minimal/Resources/Private/Language/locallang_db.xlf:crop.small.portrait',
        'allowedAspectRatios' => [
            'free' => [
                'title' => 'LLL:EXT:core/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                'value' => 0.0,
            ],
        ],
    ],
    'small_landscape' => [
        'title'               => 'LLL:EXT:base_minimal/Resources/Private/Language/locallang_db.xlf:crop.small.landscape',
        'allowedAspectRatios' => [
            'free' => [
                'title' => 'LLL:EXT:core/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                'value' => 0.0,
            ],
        ],
    ],
    'medium' => [
        'title'               => 'LLL:EXT:base_minimal/Resources/Private/Language/locallang_db.xlf:crop.medium',
        'allowedAspectRatios' => [
            'free' => [
                'title' => 'LLL:EXT:core/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                'value' => 0.0,
            ],
        ],
    ],
];
