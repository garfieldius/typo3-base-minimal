<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\DataProcessing;

use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Add cObject data and the page record as variables
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CommonVariablesProcessor implements DataProcessorInterface
{
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $processedData['data'] = $cObj->data;
        $processedData['page'] = $this->getPageDataArray($GLOBALS['TSFE']->page);
        $processedData['rootline'] = array_map((function (array $page): array {
            return $this->getPageDataArray($page);
        })->bindTo($this), $GLOBALS['TSFE']->rootLine);

        return $processedData;
    }

    private function getPageDataArray(array $page): array
    {
        static $fields = [
            'uid',
            'pid',
            'title',
            'subtitle',
            'nav_title',
            'doktype',
            'backend_layout',
        ];
        $result = [];

        foreach ($fields as $field) {
            if (!empty($page[$field])) {
                $result[$field] = $page[$field];
            }
        }

        return $result;
    }
}
