<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\DataProcessing;

use Psr\Http\Message\RequestInterface;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Add the current language of the site as variable
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class SiteLanguageProcessor implements DataProcessorInterface
{
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $request = $GLOBALS['TYPO3_REQUEST'] ?? null;

        if ($request instanceof RequestInterface && $request->getAttribute('language')) {
            $language = $request->getAttribute('language');

            if ($language instanceof SiteLanguage) {
                $as = $processorConfiguration['as'] ?? 'language';
                $processedData[$as] = $language->toArray();
            }
        }

        return $processedData;
    }
}
