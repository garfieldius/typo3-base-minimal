<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\Tests\DataProcessing;

use GrossbergerGeorg\BaseMinimal\DataProcessing\CommonVariablesProcessor;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * CommonVariablesProcessorTest
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CommonVariablesProcessorTest extends TestCase
{
    public function testProcess()
    {
        $data = [
            'uid' => 1,
            'abc' => 'asd',
        ];

        $page = [
            'uid' => 2,
            'abc' => 'asd',
        ];

        $parent = [
            'uid' => 3,
            'abc' => 'asd',
        ];

        $cObj = $this->createMock(ContentObjectRenderer::class);
        $cObj->data = $data;

        $GLOBALS['TSFE'] = new \stdClass();
        $GLOBALS['TSFE']->page = $page;
        $GLOBALS['TSFE']->rootLine = [$page, $parent];

        $expected = [
            'data'     => $data,
            'page'     => ['uid' => $page['uid']],
            'rootline' => [['uid' => $page['uid']], ['uid' => $parent['uid']]],
        ];

        $subject = new CommonVariablesProcessor();
        $actual = $subject->process($cObj, [], [], []);

        static::assertEquals($expected, $actual);
    }
}
