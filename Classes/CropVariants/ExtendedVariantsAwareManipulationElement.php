<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\CropVariants;

use TYPO3\CMS\Backend\Form\Element\ImageManipulationElement;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class ExtendedVariantsAwareManipulationElement extends ImageManipulationElement
{
    private CropVariantsLoader $cropVariantsLoader;

    protected function populateConfiguration(array $baseConfiguration)
    {
        $config = parent::populateConfiguration($baseConfiguration);
        $config['cropVariants'] = $this->extendCropVariants();

        return $config;
    }

    private function extendCropVariants(): array
    {
        $this->cropVariantsLoader ??= GeneralUtility::makeInstance(CropVariantsLoader::class);
        $parent = [];

        if (!empty($this->data['inlineParentUid'])) {
            $parent = BackendUtility::getRecord(
                $this->data['inlineParentTableName'],
                $this->data['inlineParentUid']
            ) ?? [];
        }

        $variants = [];
        $configured = $this->cropVariantsLoader->getForRecord(
            $this->data['inlineParentTableName'] ?? $this->data['tableName'] ?? 'default',
            $this->data['inlineParentFieldName'] ?? 'default',
            $parent,
            $this->data['databaseRow'] ?? []
        );

        foreach ($configured as $i => $crop) {
            $variants[$i] = [
                'title'               => $crop['title'],
                'selectedRatio'       => key($crop['aspects']),
                'cropArea'            => $crop['cropArea'],
                'allowedAspectRatios' => $crop['aspects'],
            ];
        }

        return $variants;
    }
}
