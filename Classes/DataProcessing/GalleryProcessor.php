<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\DataProcessing;

use GrossbergerGeorg\BaseMinimal\CropVariants\GalleryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Gallery
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class GalleryProcessor implements DataProcessorInterface
{
    private GalleryBuilder $galleryBuilder;

    /**
     * @param GalleryBuilder $galleryBuilder
     */
    public function __construct(GalleryBuilder $galleryBuilder = null)
    {
        $this->galleryBuilder = $galleryBuilder ?? GeneralUtility::makeInstance(GalleryBuilder::class);
    }

    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $this->galleryBuilder->setTable($processorConfiguration['table'] ?? 'tt_content');
        $this->galleryBuilder->setField($processorConfiguration['field'] ?? 'image');

        $gallery = $this->galleryBuilder->createGallery(
            $processedData,
            $processorConfiguration['rendererOptions'] ?? []
        );
        $as = $processorConfiguration['as'] ?? 'gallery';

        $processedData[$as] = $gallery;

        return $processedData;
    }
}
