<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\CropVariants;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class Variant implements \JsonSerializable
{
    private array $urls = [];

    private int $width;

    private int $height;

    private string $mediaQuery;

    private int $columns;

    /**
     * Variant constructor.
     * @param int $width
     * @param int $height
     * @param string $mediaQuery
     * @param int $columns
     */
    public function __construct(int $width, int $height, string $mediaQuery, int $columns)
    {
        $this->width = $width;
        $this->height = $height;
        $this->mediaQuery = $mediaQuery;
        $this->columns = $columns;
    }

    /**
     * @return array
     */
    public function getUrls(): array
    {
        return $this->urls;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return string
     */
    public function getMediaQuery(): string
    {
        return $this->mediaQuery;
    }

    /**
     * @return int
     */
    public function getColumns(): int
    {
        return $this->columns;
    }

    public function addUrl(VariantUrl $url): void
    {
        $this->urls[$url->getScaleFactor() . 'x'] = $url;
    }

    public function jsonSerialize()
    {
        $urls = [];

        foreach ($this->urls as $key => $url) {
            $urls[$key] = $url->jsonSerialize();
        }

        return [
            'urls'       => $urls,
            'width'      => $this->getWidth(),
            'height'     => $this->getHeight(),
            'mediaQuery' => $this->getMediaQuery(),
            'columns'    => $this->getColumns(),
        ];
    }
}
