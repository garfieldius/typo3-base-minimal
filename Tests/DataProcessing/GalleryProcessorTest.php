<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\Tests\DataProcessing;

use GrossbergerGeorg\BaseMinimal\CropVariants\GalleryBuilder;
use GrossbergerGeorg\BaseMinimal\DataProcessing\GalleryProcessor;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class GalleryProcessorTest extends TestCase
{
    public function testProcess()
    {
        $record = [
            'uid' => 1,
            'pid' => 2,
        ];
        $field = 'field';
        $table = 'table';
        $options = ['fast' => true];

        $result = ['1' => '2'];

        $builder = $this->createMock(GalleryBuilder::class);
        $builder->expects(static::once())->method('createGallery')->with(
            $record,
            $options
        )->willReturn($result);

        $builder->expects(static::once())->method('setTable')->with(static::equalTo($table));
        $builder->expects(static::once())->method('setField')->with(static::equalTo($field));

        $cObj = (new \ReflectionClass(ContentObjectRenderer::class))->newInstanceWithoutConstructor();
        $cObj->data = $record;
        $subject = new GalleryProcessor($builder);

        $processorOptions = [
            'rendererOptions' => $options,
            'table'           => $table,
            'field'           => $field,
        ];

        $expected = ['gallery' => $result];
        $actual = $subject->process($cObj, [], $processorOptions, []);

        static::assertSame($expected, $actual);
    }
}
