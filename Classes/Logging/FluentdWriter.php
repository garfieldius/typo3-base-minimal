<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\Logging;

use Fluent\Logger\FluentLogger;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class FluentdWriter extends NormalizedArrayWriter
{
    protected string $host;

    protected int $port;

    protected string $tag = 'app.typo3';

    /**
     * @var FluentLogger
     */
    private $writer;

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @param int $port
     */
    public function setPort(int $port): void
    {
        $this->port = $port;
    }

    /**
     * @param string $tag
     */
    public function setTag(string $tag): void
    {
        $this->tag = $tag;
    }

    protected function writeData(array $data): void
    {
        $this->writer ??= new FluentLogger($this->host, $this->port);
        $this->writer->post($this->tag, $data);
    }
}
