<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

// Set bootstrap classes as options of the Space before/after dropdown
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][1][1] = 'mt-1';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][2][1] = 'mt-2';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][3][1] = 'mt-3';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][4][1] = 'mt-4';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][5][1] = 'mt-5';

$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][1][1] = 'mb-1';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][2][1] = 'mb-2';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][3][1] = 'mb-3';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][4][1] = 'mb-4';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][5][1] = 'mb-5';

// Remove bullets CE (it's not 2001 anymore!!)
unset($GLOBALS['TCA']['tt_content']['types']['bullets']);

foreach ($GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'] as $i => $item) {
    if ($item[1] == 'bullets') {
        unset($GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][$i]);
        break;
    }
}

unset($i, $item);
