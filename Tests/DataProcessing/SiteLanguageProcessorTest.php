<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\Tests\DataProcessing;

use GrossbergerGeorg\BaseMinimal\DataProcessing\SiteLanguageProcessor;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class SiteLanguageProcessorTest extends TestCase
{
    protected function setUp(): void
    {
        unset($GLOBALS['TYPO3_REQUEST']);
    }

    /**
     * @dataProvider processData
     * @param ServerRequest|null $request
     * @param SiteLanguage|null $expectedLanguage
     */
    public function testProcess(?ServerRequest $request, ?SiteLanguage $expectedLanguage)
    {
        $cObj = $this->createMock(ContentObjectRenderer::class);
        $expected = [];
        $as = 'language';

        if ($expectedLanguage) {
            $expected[$as] = $expectedLanguage->toArray();
        }

        if ($request) {
            $GLOBALS['TYPO3_REQUEST'] = $request;
        }

        $subject = new SiteLanguageProcessor();
        $actual = $subject->process($cObj, [], ['as' => $as], []);

        static::assertEquals($expected, $actual);
    }

    public function processData()
    {
        $datasets = [];

        $datasets['No request, no language'] = [null, null];

        $request1 = $this->createMock(ServerRequest::class);
        $request1->expects(static::any())->method('getAttribute')->with('language')->willReturn(null);

        $datasets['With request, no language'] = [$request1, null];

        $language = new SiteLanguage(0, 'en', new Uri('http://localhost/de'), []);

        $request2 = $this->createMock(ServerRequest::class);
        $request2->expects(static::any())->method('getAttribute')->with('language')->willReturn($language);

        $datasets['With request, with language'] = [$request2, $language];

        return $datasets;
    }
}
