<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\CropVariants;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class VariantUrl implements \JsonSerializable
{
    private int $scaleFactor;

    private string $url;

    /**
     * VariantUrl constructor.
     * @param int $scaleFactor
     * @param string $url
     */
    public function __construct(int $scaleFactor, string $url)
    {
        $this->scaleFactor = $scaleFactor;
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getScaleFactor(): int
    {
        return $this->scaleFactor;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    public function jsonSerialize()
    {
        return [
            'url'   => $this->getUrl(),
            'scale' => $this->getScaleFactor(),
        ];
    }
}
