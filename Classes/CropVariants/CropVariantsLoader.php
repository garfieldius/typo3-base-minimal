<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\CropVariants;

use Symfony\Component\Yaml\Yaml;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Package\PackageManager;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * CropVariantsResolver
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CropVariantsLoader implements SingletonInterface
{
    private $configuration = [];

    public function __construct(CacheManager $cacheManager, PackageManager $packageManager)
    {
        $cache = $cacheManager->getCache('base_minimal');
        $config = $cache->get('crop');

        if (is_array($config)) {
            $this->configuration = $config;
        } else {
            $config = [];

            foreach ($packageManager->getActivePackages() as $package) {
                $file = $package->getPackagePath() . 'Configuration/CropVariants.yml';

                if (is_file($file)) {
                    $data = Yaml::parseFile($file);

                    if (is_array($data)) {
                        ArrayUtility::mergeRecursiveWithOverrule($config, $data);
                    }
                }
            }

            $default = $config['default'];
            unset($config['default']);
            $this->configuration['default'] = $default;

            foreach ($config as $key => $settings) {
                if ($key[0] !== '.') {
                    $overrides = $default;
                    ArrayUtility::mergeRecursiveWithOverrule($overrides, $settings);
                    $this->configuration[$key] = $overrides;
                }
            }

            $this->removeUnsets($this->configuration);
            $cache->set('crop', $this->configuration);
        }
    }

    public function getForRecord(string $table, string $field, array $parent, array $self): array
    {
        $config = $this->configuration['default'];

        foreach ($this->configuration as $k => $overrides) {
            if (($overrides['table'] ?? null) === $table && ($overrides['field'] ?? null) === $field) {
                $requiredMatches = 0;
                $actualMatches = 0;

                foreach (['self' => $self, 'parent' => $parent] as $section => $record) {
                    foreach ($overrides['_if'][$section] ?? [] as $column => $expectedValue) {
                        $requiredMatches++;

                        if (isset($record[$column])) {
                            $actualValue = $record[$column];

                            if (!is_array($actualValue)) {
                                $actualValue = [$actualValue];
                            }

                            if (!in_array($expectedValue, $actualValue)) {
                                $actualMatches++;
                            }
                        }
                    }
                }

                if ($requiredMatches === $actualMatches) {
                    $config = $overrides;
                }
            }
        }

        unset($config['table'], $config['field'], $config['_if']);

        return $config;
    }

    public function getVariantSet(string $name): array
    {
        $set = $this->configuration[$name] ?? null;

        if (!is_array($set) || empty($set)) {
            throw new \InvalidArgumentException('No variant set with name ' . $name);
        }

        return $set;
    }

    private function removeUnsets(array &$subject)
    {
        foreach ($subject as $key => $value) {
            if ($value === '__UNSET') {
                unset($subject[$key]);
            } elseif (is_array($value)) {
                $this->removeUnsets($subject[$key]);
            }
        }
    }
}
