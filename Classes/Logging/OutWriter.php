<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\Logging;

/**
 * Writer for stdout or stderr
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class OutWriter extends NormalizedArrayWriter
{
    private static $handles = [];

    protected function writeData(array $data): void
    {
        $line = json_encode($data) . "\n";
        static::$handles['php://stderr'] ??= fopen('php://stderr', 'ab');

        if (static::$handles['php://stderr'] !== false) {
            fwrite(static::$handles['php://stderr'], $line);
        }
    }
}
