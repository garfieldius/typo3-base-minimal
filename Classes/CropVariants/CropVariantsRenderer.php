<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\CropVariants;

use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\Rendering\RendererRegistry;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\ImageService;

/**
 * CropVariantsRenderer
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CropVariantsRenderer
{
    private array $rendererOptions = [
        'autoplay'             => null,
        'controls'             => null,
        'muted'                => null,
        'loop'                 => null,
        'allow'                => null,
        'modestbranding'       => true,
        'relatedVideos'        => false,
        'no-cookie'            => true,
        'showinfo'             => false,
        'additionalAttributes' => ['class' => 'embed-responsive-item'],
    ];

    /**
     * @var RendererRegistry
     */
    private RendererRegistry $rendererRegistry;
    /**
     * @var ImageService
     */
    private ImageService $imageService;
    /**
     * @var CropVariantsLoader
     */
    private CropVariantsLoader $loader;

    /**
     * CropVariantsRenderer constructor.
     * @param RendererRegistry $rendererRegistry
     * @param ImageService $imageService
     * @param CropVariantsLoader $loader
     */
    public function __construct(
        RendererRegistry $rendererRegistry,
        ImageService $imageService,
        CropVariantsLoader $loader
    ) {
        $this->rendererRegistry = $rendererRegistry;
        $this->imageService = $imageService;
        $this->loader = $loader;
    }

    public function generateVariants(array $references, string $variantSet): array
    {
        $variants = $this->loader->getVariantSet($variantSet);
        $results = [];

        /** @var FileReference $reference */
        foreach ($references as $reference) {
            $result = $this->generateVariant($reference, $variants);
            $results[] = $result;
        }

        return $results;
    }

    public function mergeOptions(array $options): void
    {
        ArrayUtility::mergeRecursiveWithOverrule($this->rendererOptions, $options);
    }

    /**
     * @param FileReference $reference
     * @param array $variants
     * @return FileReference[]
     */
    public function generateVariant(FileReference $reference, array $variants): array
    {
        $result = [
            'original' => $reference,
        ];
        $renderer = $this->rendererRegistry->getRenderer($reference);

        if ($renderer) {
            $result['html'] = $renderer->render(
                $reference,
                $this->rendererOptions['mediaWidth'],
                $this->rendererOptions['mediaHeight'],
                $this->rendererOptions
            );
        } else {
            $cropVariantCollection = CropVariantCollection::create((string) $reference->getProperty('crop'));

            foreach ($variants as $name => $config) {
                $width = (int) $config['width'];
                $height = (int) $config['height'];
                $cropArea = $cropVariantCollection->getCropArea($name);
                $crop = $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($reference);
                $processingInstructions = [
                    'width'  => $width . 'm',
                    'height' => $height . 'm',
                    'crop'   => $crop,
                ];

                $processedImage = $this->imageService->applyProcessingInstructions(
                    $reference,
                    $processingInstructions
                );

                $varient = new Variant(
                    (int) $processedImage->getProperty('width'),
                    (int) $processedImage->getProperty('height'),
                    (string) $config['mediaQuery'],
                    (int) $config['columns']
                );

                $varient->addUrl(new VariantUrl(1, $this->imageService->getImageUri($processedImage)));

                $result[$name] = $varient;

                foreach (GeneralUtility::intExplode(',', $config['scaleFactors']) as $scaling) {
                    if ($scaling < 1 || $scaling > 6) {
                        continue;
                    }

                    $processingInstructions = [
                        'width'  => ($width * $scaling) . 'm',
                        'height' => ($height * $scaling) . 'm',
                        'crop'   => $crop,
                    ];
                    $processedImage = $this->imageService->applyProcessingInstructions(
                        $reference,
                        $processingInstructions
                    );
                    $varient->addUrl(new VariantUrl($scaling, $this->imageService->getImageUri($processedImage)));
                }
            }
        }

        return $result;
    }
}
