<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\Logging;

use Monolog\Formatter\NormalizerFormatter;
use TYPO3\CMS\Core\Log\LogRecord;
use TYPO3\CMS\Core\Log\Writer\AbstractWriter;

/**
 * Base for writer that processes a monolog normalized
 * log record
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
abstract class NormalizedArrayWriter extends AbstractWriter
{
    /**
     * @var NormalizerFormatter
     */
    private $normalizer;

    public function writeLog(LogRecord $record)
    {
        $this->normalizer ??= new NormalizerFormatter('Y.m.d H:i:s');
        $this->writeData($this->normalizer->format($record->toArray()));
    }

    abstract protected function writeData(array $data): void;
}
