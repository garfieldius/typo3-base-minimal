<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\BaseMinimal\CropVariants;

use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\FileRepository;

/**
 * GalleryService
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class GalleryBuilder
{
    /**
     * @var string
     */
    private string $table;

    /**
     * @var string
     */
    private string $field;

    /**
     * @var CropVariantsLoader
     */
    private CropVariantsLoader $variantsLoader;

    /**
     * @var CropVariantsRenderer
     */
    private CropVariantsRenderer $renderer;

    /**
     * @var FileRepository
     */
    private FileRepository $filesRepository;

    /**
     * GalleryBuilder constructor.
     * @param CropVariantsLoader $variantsLoader
     * @param CropVariantsRenderer $renderer
     * @param FileRepository $filesRepository
     */
    public function __construct(
        CropVariantsLoader $variantsLoader,
        CropVariantsRenderer $renderer = null,
        FileRepository $filesRepository = null
    ) {
        $this->variantsLoader = $variantsLoader;
        $this->renderer = $renderer;
        $this->filesRepository = $filesRepository;
    }

    /**
     * @param string $table
     */
    public function setTable(string $table): void
    {
        $this->table = $table;
    }

    /**
     * @param mixed $field
     */
    public function setField($field): void
    {
        $this->field = $field;
    }

    public function createGallery(
        array $record,
        array $renderOptions,
        array $references = []
    ): array {
        $items = $this->createList($record, $renderOptions, $references);
        $result = [];

        while (count($items)) {
            $variants = array_shift($items);
            /** @var Variant $item */
            foreach ($variants as $name => $item) {
                if (empty($result[$name])) {
                    $result[$name] = [0 => []];
                }

                $result[$name][count($result[$name]) - 1][] = $item;

                if ($items
                    && $name !== 'html'
                    && $name !== 'original'
                    && count($result[$name]) >= $item->getColumns()) {
                    $result[$name][] = [];
                }
            }
        }

        return $result;
    }

    public function createList(
        array $record,
        array $renderOptions,
        array $references = []
    ): array {
        if (!$references) {
            $references = $this->filesRepository->findByRelation(
                $this->table,
                $this->field,
                $record['uid']
            );
        } else {
            $references = array_map(static function ($reference) {
                if (method_exists($reference, 'getOriginalResource')) {
                    $reference = $reference->getOriginalResource();
                }

                return $reference;
            }, $references);
        }

        if (count($references) < 1) {
            return [];
        }

        $this->renderer->mergeOptions($renderOptions);
        $elements = [];

        /** @var FileReference $reference */
        foreach ($references as $reference) {
            $variants = $this->variantsLoader->getForRecord(
                $this->table,
                $this->field,
                $reference->getReferenceProperties(),
                $record
            );

            $data = $this->renderer->generateVariant($reference, $variants);

            if ($data) {
                $elements[] = $data;
            }
        }

        return $elements;
    }
}
